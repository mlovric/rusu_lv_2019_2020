while True:
    try:
        number = float(input("unesi broj:"))
        break
    except ValueError:
        print ("pogresan unos, pokusaj ponovno")

if number >=0.9:
    print ("uneseni broj %.2f pripada kategoriji A" %(number))
elif number >=0.8 and number <0.9:
    print ("uneseni broj %.2f pripada kategoriji B" %(number))
elif number >=0.7 and number <0.8:
    print ("uneseni broj %.2f pripada kategoriji C" %(number))
elif number >=0.6 and number <0.7:
    print ("uneseni broj %.2f pripada kategoriji D" %(number))
elif number <0.6:
    print ("uneseni broj %.2f pripada kategoriji F" %(number))
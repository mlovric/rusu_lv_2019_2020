import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#s1 = pd.Series(['crvenkapica', 'baka', 'majka', 'lovac', 'vuk'])
#print(s1)
#s2 = pd.Series(5., index=['a', 'b', 'c', 'd', 'e'], name = 'ime_objekta')
#print(s2)
#print(s2['b'])
#s3 = pd.Series(np.random.randn(5))
#print(s3)
#print(s3[3])

#data = {'year': [2010, 2011, 2012, 2011, 2012, 2010, 2011, 2012],
#'team': ['Bears', 'Bears', 'Bears', 'Packers', 'Packers', 'Lions', 'Lions', 'Lions'],
#'wins': [11, 8, 10, 15, 11, 6, 10, 4],
#'losses': [5, 8, 6, 1, 5, 10, 6, 12]}
#football = pd.DataFrame(data, columns=['year', 'team', 'wins', 'losses'])
#print(football)

#mtcars = pd.read_csv('mtcars.csv')
#print(len(mtcars))
#print(mtcars)

#print(mtcars.head(5))
#print(mtcars.tail(3))
#print(mtcars.info())
#print(mtcars.describe())

print(mtcars['car'])
print(mtcars.cyl)
print(mtcars.cyl > 6)
print(mtcars[mtcars.cyl > 6])
print(mtcars[(mtcars.cyl == 4) & (mtcars.hp > 100)].car)
print(mtcars[['car','cyl']])
print(mtcars.cyl[2:4])
print(mtcars[5:12])
print(mtcars.mpg[3:5])
mtcars['jedinice'] = np.ones(len(mtcars))
mtcars['heavy'] = mtcars.wt > 4.5
print(mtcars[['car','heavy']])
print(mtcars.query('cyl == [4,6]').car)
print(mtcars.iloc[1:3, 5:10])
print(mtcars.iloc[:, 3:5])
print(mtcars.iloc[:, [0,4,7]])
print(mtcars.iloc[[1,29], :])

new_mtcars = mtcars.groupby('cyl')
print(new_mtcars.count())
print(new_mtcars.sum())
print(new_mtcars.mean())
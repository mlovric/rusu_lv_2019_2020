# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd   #za ucitavanje .csv datoteke
 
mtcars = pd.read_csv('mtcars.csv') 
cilindar4= mtcars[mtcars.cyl == 4]  
cilindar6= mtcars[mtcars.cyl == 6]
cilindar8= mtcars[mtcars.cyl == 8]
 
index1=np.arange(0,len(cilindar4),1) 
index2=np.arange(0,len(cilindar6),1)  
index3=np.arange(0,len(cilindar8),1)  
 
 
width = 0.25 
 

plt.figure()
 
plt.bar(index1, cilindar4["mpg"],width, color=(1,0,0)) 
plt.bar(index2 + width, cilindar6["mpg"],width, color=(0,1,0.3))
plt.bar(index2 + 2*width, cilindar6["mpg"],width, color=(0,0,1))
plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel('auto')
plt.ylabel('mpg')
plt.grid(axis='y',linestyle='--')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2) 
 

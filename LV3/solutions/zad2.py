 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd   #za ucitavanje .csv datoteke
 
mtcars = pd.read_csv('../resources/mtcars.csv') 
cilindar4= mtcars[mtcars.cyl == 4]  
cilindar6= mtcars[mtcars.cyl == 6]
cilindar8= mtcars[mtcars.cyl == 8]
 
index1=np.arange(0,len(cilindar4),1) 
index2=np.arange(0,len(cilindar6),1)  
index3=np.arange(0,len(cilindar8),1)  
 
 
width = 0.25 
 

plt.figure()
 
plt.bar(index1, cilindar4["mpg"],width, color=(1,0,0)) 
plt.bar(index2 + width, cilindar6["mpg"],width, color=(0,1,0.3))
plt.bar(index2 + 2*width, cilindar6["mpg"],width, color=(0,0,1))
plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel('auto')
plt.ylabel('mpg')
plt.grid(axis='y',linestyle='--')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2) 
 
tezina4=[]
tezina6=[]
tezina8=[]
for i in cilindar4["wt"]:
    tezina4.append(i)    
 
for i in cilindar6["wt"]:
    tezina6.append(i)   
 
for i in cilindar8["wt"]:
    tezina8.append(i)   
 
plt.figure()
plt.boxplot([tezina4, tezina6, tezina8], positions = [4,6,8])  
plt.title("Tezina automobila s 4, 6 i 8 cilindara")
plt.xlabel('Broj klipova')
plt.ylabel('Tezina wt')
plt.grid(axis='y',linestyle='--')

automatsko=mtcars[(mtcars.am== 1)]
potrosnja_aut=[]
for i in automatsko["mpg"]:
    potrosnja_aut.append(i)
     
rucno=mtcars[(mtcars.am== 0)]
potrosnja_rucno=[]
for i in rucno["mpg"]:
    potrosnja_rucno.append(i)
     
plt.figure()
plt.boxplot([potrosnja_rucno, potrosnja_aut], positions = [0,1],sym='k+')
plt.title("Potrosnja automobila s rucnim vs. automatskim mjenjacem")
plt.ylabel('miles per gallon')
plt.xlabel('0=rucni mjenjac, 1=automatski mjenjac')
plt.grid(axis='y',linestyle='--')

ubrzanje_a=[]
snaga_a=[]
ubrzanje_r=[]
snaga_r=[]

for i in automatsko ["qsec"]:
    ubrzanje_a.append(i)
    
for i in automatsko ["hp"]:
    snaga_a.append(i)
    
for i in rucno ["qsec"]:
    ubrzanje_r.append(i)
    
for i in rucno ["hp"]:
    snaga_r.append(i)
    

plt.figure()
plt.scatter(ubrzanje_a, snaga_a, marker='^')   
plt.scatter(ubrzanje_r, snaga_r, marker='d', facecolors='none', edgecolors='r')  
plt.title("Ubrzanje/Snaga automobila s rucnim vs. automatskim mjenjacem")
plt.ylabel('Snaga - hp')
plt.xlabel('Ubrzanje - qsec')
plt.legend(["Automatski mjenjac","Rucni mjenjac"])
plt.grid(linestyle='--')